function apply() {
    var L1 = document.getElementById("Ligue1").checked;
    var Liga = document.getElementById("Liga").checked;
    var PL = document.getElementById("PL").checked;

    if(L1){
        
        var elems = document.getElementsByClassName('L1');
        for (var i=0;i<elems.length;i+=1){
            elems[i].style.display = 'block';
        }
    }
    else {
        var elems = document.getElementsByClassName('L1');
        for (var i=0;i<elems.length;i+=1){
            elems[i].style.display = 'none';
        }
        
    }

    if(Liga){
        
        var elems = document.getElementsByClassName('Liga');
        for (var i=0;i<elems.length;i+=1){
            elems[i].style.display = 'block';
        }
    }
    else {
        var elems = document.getElementsByClassName('Liga');
        for (var i=0;i<elems.length;i+=1){
            elems[i].style.display = 'none';
        }
        
    }

    if(PL){
        
        var elems = document.getElementsByClassName('PL');
        for (var i=0;i<elems.length;i+=1){
            elems[i].style.display = 'block';
        }
    }
    else {
        var elems = document.getElementsByClassName('PL');
        for (var i=0;i<elems.length;i+=1){
            elems[i].style.display = 'none';
        }
        
    }
}

function afficher(){
    var suscribeModal = document.getElementById('suscribeModal');
    if (typeof suscribeModal.showModal === "function") {
        suscribeModal.showModal();
    } else {
    console.error("L'API <dialog> n'est pas prise en charge par ce navigateur.");
    }
}

function close1(){
    var suscribeModal = document.getElementById('suscribeModal');
    suscribeModal.close();
}

function connect(){
    var connectModal = document.getElementById('connectModal');
    if (typeof connectModal.showModal === "function") {
        connectModal.showModal();
    } else {
    console.error("L'API <dialog> n'est pas prise en charge par ce navigateur.");
    }
}

function close2(){
    var connectModal = document.getElementById('connectModal');
    connectModal.close();
}

function sensitization(){
    document.getElementById("mainContent").style.display = "none";
    document.getElementById("secondaryContent").style.display = "block"
}

function principal(){
    document.getElementById("mainContent").style.display = "block";
    document.getElementById("secondaryContent").style.display = "none"
}

